﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Text;

namespace DependencyInversion
{
    public class EmailSender : INotificationAction
    {
        public void ActOnNotification(string message)
        {
            string yourEmailAdress = "twojgmail@gmail.com";
            string emailPassword = "twojehaslo";
            MailAddress to = new MailAddress(yourEmailAdress);
            MailAddress from = new MailAddress(yourEmailAdress);
            MailMessage mail = new MailMessage(from, to);
            mail.Subject = "Hej Kuba Tutaj Kuba";
            mail.Body = "Testuje sobie DI";
            SmtpClient smtp = new SmtpClient();
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.Credentials = new NetworkCredential(yourEmailAdress, emailPassword);
            smtp.EnableSsl = true;
            Console.WriteLine("Wysyłanie wiadomości...");
            smtp.Send(mail);

        }
    }
}
