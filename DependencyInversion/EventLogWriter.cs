﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DependencyInversion
{
    public class EventLogWriter : INotificationAction
    {
        public void ActOnNotification(string message)
        {
            Console.WriteLine(message);
        }
    }
}
