﻿namespace DependencyInversion
{
    public interface INotificationAction
    {
        void ActOnNotification(string message);
    }
}
