﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DependencyInversion
{
    //DI przez konstruktor 
    public class IISAppPoolWatcher
    {
        INotificationAction action = null;
        public IISAppPoolWatcher(INotificationAction concreteImplementation)
        {
            this.action = concreteImplementation;
        }

        public void Notify(string message)
        {
            if (action == null)
            {
                action = new EventLogWriter();
            }
            action.ActOnNotification(message);
        }
    }

    //DI przez metode
    public class IISAppPoolWatcher2
    {
        INotificationAction action = null;
        public void Notify(INotificationAction concreteAction, string message)
        {
            this.action = concreteAction;
            action.ActOnNotification(message);
        }
        public void Notify(string message)
        {
            if (action == null)
            {
                action = new EventLogWriter();
            }
            action.ActOnNotification(message);
        }
    }

}
