﻿using System;

namespace DependencyInversion
{
   

    class Program
    {
        static void Main(string[] args)
        {
            EventLogWriter writer = new EventLogWriter();
            EmailSender sender = new EmailSender();

            //DI przez konstruktor 
            IISAppPoolWatcher watcher1 = new IISAppPoolWatcher(writer);
            IISAppPoolWatcher watcher2 = new IISAppPoolWatcher(sender);

            IISAppPoolWatcher2 watcher3 = new IISAppPoolWatcher2();
            

            watcher1.Notify("Wysyłanie wiadomosci do uzytkownika");
            watcher2.Notify("Wysłano");

            //DI przez metode
            watcher3.Notify(writer, "Wysyłanie wiadomosci do uzytkownika");
            watcher3.Notify(sender, "Wysłano");


            Console.ReadKey();
        }
    }
}
