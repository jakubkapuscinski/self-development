﻿using System;

namespace DisposableImplementation
{
    public class MyClass : IDisposable
    {
        private bool IsDisposed = false; //flaga czy oczyszczono
        
        protected void Dispose(bool Disposing)
        {
            if (!IsDisposed)
            {
                if (Disposing)
                {
                    GC.Collect();
                }
            }

            IsDisposed = true;
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        ~MyClass()
        {
            Dispose(false);
        }
    }
}
