﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;

namespace ExtensionMethods
{
    public static class MyExtensions
    {
        public static string VowelsFirst(this string s)
        {
            if (string.IsNullOrEmpty(s)) throw new ArgumentNullException("Your variable can't be empty");

            string onlyConsonants = Regex.Replace(s, "[aeiouy]", "", RegexOptions.IgnoreCase);
            string onlyVowels= Regex.Replace(s, "[bcdfghjklmnpqrstvwxyz]", "", RegexOptions.IgnoreCase); ;
           
            return onlyVowels + onlyConsonants;
        }

        public static IEnumerable<TSource> WhereIsMyItem<TSource>(this IEnumerable<TSource> source, Func<TSource, bool> predicate)
        {
            foreach (var item in source)
            {
                if (predicate(item)) yield return item;
            }
        }


    }
}
