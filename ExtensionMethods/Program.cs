﻿using System;
using System.Collections;
using System.Linq;

namespace ExtensionMethods
{
    class Program
    {
        static void Main(string[] args)
        {
            var people = new[]
            {
               new  {Name = "Jakub", Age = 22, Email = "jakub@gmail.com" },
               new  {Name = "Kasia", Age = 25, Email = "kasia@gmail.com" },
               new  {Name = "Robert", Age = 31, Email = "robert@wp.pl" },
               new  {Name = "Marysia", Age = 12, Email = "marysia@niepodam.pl" },
               new  {Name = "Jakub", Age = 56, Email = "szymon@wp.pl" }
            };

            IEnumerable specifiedPersons = people
                .WhereIsMyItem(n => n.Age > 30)
                .OrderByDescending(n => n.Name)
                .Select(n => n.Name.ToUpper().VowelsFirst());
            
            foreach (var person in specifiedPersons)
            {
                Console.WriteLine(person);
            }

        }
    }
}
