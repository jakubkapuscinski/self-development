﻿namespace FactoryPattern
{
    public interface IPaymentGateway
    {
        void MakePayment(Product product);
    }
}