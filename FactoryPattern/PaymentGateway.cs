﻿using static FactoryPattern.BanksPayments;

namespace FactoryPattern
{
    public class PaymentGateway
    {
        public virtual IPaymentGateway CreatePaymentGateway(EPaymentMethod method)
        {
            IPaymentGateway gateway = null;

            switch(method)
            {
                case EPaymentMethod.BANK_ONE:
                    gateway = new BankOne();
                    break;
                case EPaymentMethod.BANK_TWO:
                    gateway = new BankTwo();
                    break;
                default:
                    break;
            }

            return gateway;
        }
    }
}
