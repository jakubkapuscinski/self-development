﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;

namespace FilesCreatingAsync
{
    class Program
    {
        static void Main(string[] args)
        {
            var watch = Stopwatch.StartNew();
            SaveFile2("A").ConfigureAwait(false);
            watch.Stop();

            Console.WriteLine("Elapsed Time 1: " + watch.ElapsedTicks);

            var watch2 = Stopwatch.StartNew();

            SaveFile("A").ConfigureAwait(false);/// dzieki ConfigureAwait nie musze czekac na watek na ktorym oryginalnie 
                                                ///zaczalem proces zamiast tego jesli watek bedzie zajety stworze nowy watek na ktorym dokoncze prace2
            SaveFile("B").ConfigureAwait(false);
            SaveFile("C").ConfigureAwait(false);

            watch2.Stop();

            Console.WriteLine("Elapsed Time 2: " + watch2.ElapsedTicks);
        }

        public static async Task SaveFile(string letter)
        {
            using (StreamWriter sw = File.AppendText(@"Path to file"))
            {
                for (int i = 0; i < 333; i++)
                {
                    await sw.WriteLineAsync(letter);
                }
            }
        }

        public static async Task SaveFile2(string letter)
        {
            using (StreamWriter sw = File.AppendText(@"Path to file"))
            {
                for (int i = 0; i < 1000; i++)
                {
                    await sw.WriteLineAsync(letter);
                }
            }
        }
    }
}
