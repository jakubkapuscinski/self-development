﻿using System;
using System.Diagnostics;
using System.IO;
using System.Threading;

namespace FilesCreatingParallel
{
    /// <summary>
    /// skoro kazdy watek otwiera i zamyka plik to 3 watki robia to dluzej
    /// </summary>
    class Program
    {
        private static readonly object lockObject = new object();
        static void Main(string[] args)
        {

            Thread thread1 = new Thread(() => SaveFile2("A"));

            Thread thread2 = new Thread(() => SaveFile("A"));
            Thread thread3 = new Thread(() => SaveFile("B"));
            Thread thread4 = new Thread(() => SaveFile("C"));

            var watch = Stopwatch.StartNew();
            thread1.Start();
            watch.Stop();

            Console.WriteLine("Elapsed Time 1: " + watch.ElapsedTicks);

            var watch2 = Stopwatch.StartNew();

            thread2.Start();
            thread3.Start();
            thread4.Start();

            watch2.Stop();

            Console.WriteLine("Elapsed Time 2: " + watch2.ElapsedTicks);
        }

        public static void SaveFile(string letter)
        {
            lock (lockObject) /// gdybym nie uzyl locka doszloby do deadlock czyli dwa watki chcialyby uzyskac dostep do tego samego pliku i czekalyby nawzajem na siebie w nieskonczonsc
            {
                using (StreamWriter sw = File.AppendText(@"PathToSaveFile"))
                {
                    for (int i = 0; i < 333; i++)
                    {
                        sw.WriteLine(letter);

                    }
                }
            }
        }

        public static void SaveFile2(string letter)
        {
            using (StreamWriter sw = File.AppendText(@"PathToSaveFile"))
            {
                for (int i = 0; i < 1000; i++)
                {
                    sw.WriteLine(letter);
                }
            }
        }
    }
}
