﻿using System;
using System.IO;
using System.Linq;
using System.Threading;

namespace FilesCreatorThreadPool
{
    public class FilesCreator
    {
        private static Random random = new Random();
        private ManualResetEvent _doneEvent;
        public string PathName { get; }
        public int TextLenght { get; private set; }

        public FilesCreator(string pathName, int textLenght, ManualResetEvent doneEvent)
        {
            PathName = pathName;
            TextLenght = textLenght;
            _doneEvent = doneEvent; /// stan watku
        }

        public void ThreadPoolCallback(Object threadContext)
        {
            int threadIndex = (int)threadContext;
            Console.WriteLine("Thread " + threadIndex + " started...");

            var randomText = CreateRandomText(TextLenght);
            File.WriteAllText(PathName, randomText);

            Console.WriteLine("Thread " + threadIndex + " file created...");
            _doneEvent.Set();/// jesli jakis thread czeka wyslij sygnal ze skonczyles juz prace
        }

        public string CreateRandomText(int textLenght)
        {
            const string charsPool = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
            return new string(Enumerable.Repeat(charsPool, textLenght)
            .Select(s => s[random.Next(s.Length)]).ToArray());
        }
    }
}
