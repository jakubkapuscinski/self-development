﻿using System;
using System.Threading;

namespace FilesCreatorThreadPool
{
    class Program
    {
        private static Random random = new Random();
        static void Main(string[] args)
        {
            const string pathToSaveFile = @"C:\Users\jkapuscinski\Desktop\Zadania\08.10.2019\FilesCreatorThreadPool\file";
            const int amountOfFiles = 5;

            var doneEvents = new ManualResetEvent[amountOfFiles]; //tworzę tablicę ukończonych operacji na wątkach
            var filesArray = new FilesCreator[amountOfFiles]; //tworzę tablicę 

            Console.WriteLine("Launching " + amountOfFiles + " tasks...");

            for (int i = 0; i < amountOfFiles; i++)
            {
                doneEvents[i] = new ManualResetEvent(false); /// startuje po watku dla kazdego pliku

                var f = new FilesCreator(pathToSaveFile + i + ".txt", random.Next(100, 300), doneEvents[i]);
                filesArray[i] = f;

                ThreadPool.QueueUserWorkItem(f.ThreadPoolCallback, i);/// kolejkuje metode do wykonania w threadpoolu
            }

            WaitHandle.WaitAll(doneEvents);/// poczekaj dopoki wszystkie watki nie zostana zwolnione
            Console.WriteLine("All files have been created.");
        }
    }
}
