let itemInput = document.getElementById("new-item");
let quantityInput = document.getElementById("new-price");
let itemsToBuyHolder = document.getElementById("items-to-buy");
window.localStorage.setItem("boughtCounter",0);

window.onload = function checkLocalStorage() {
  const items = { ...localStorage };

  Object.keys(items).forEach(function(item) {
    if (item.startsWith("Item")) {
      let fullText = items[item]; 
      let itemName = fullText.replace(/[0-9]/g, "");
      let itemQuantity = fullText.replace(/\D/g, "");
      let newItem = this.createNewItem(itemName, itemQuantity);
      itemsToBuyHolder.appendChild(newItem);
    } else if (item.startsWith("Bought")) {
      let boughtItems = document.getElementById("bought-items");
      let itemsList = document.createElement("li");
      itemsList.innerText = items[item];
      boughtItems.appendChild(itemsList);
    }
  });
};

function createNewItem(item, price) {
  let itemsList = document.createElement("li");
  itemsList.setAttribute("class", "item");
  itemsList.setAttribute("class","list-group-item");
  let label = document.createElement("label");

  label.innerText = item + " " + price;

  let deleteButton = document.createElement("button");
  deleteButton.setAttribute("class", "remove");
  deleteButton.innerText = "X";

  let editButton = document.createElement("button");
  editButton.setAttribute("class", "edit");
  editButton.innerText = "Edit";

  deleteButton.onclick = function() {
    deleteItem();
  };

  editButton.onclick = function() {
    editItem();
  };

  itemsList.appendChild(label);
  itemsList.appendChild(deleteButton);
  itemsList.appendChild(editButton);

  return itemsList;
}

function addItem() {
  let newItem = createNewItem(itemInput.value, quantityInput.value);
  itemsToBuyHolder.appendChild(newItem);

  let itemCounter = localStorage.length;
  let label = newItem.getElementsByTagName("label");
  localStorage.setItem("Item" + itemCounter, label[0].innerText);
  console.log(localStorage.getItem("Item" + itemCounter));
  itemCounter++;

  itemInput.value = "";
  quantityInput.value = "";
}

function deleteItem() {
  let items = document.getElementsByClassName("remove");
  for (let i = 0; i < items.length; i++) {
    items[i].onclick = function() {
      let parent = this.parentElement;
      alreadyBought(parent);
      parent.remove();
    };
  }
}

function editItem() {
  let items = document.getElementsByClassName("edit");
  for (let i = 0; i < items.length; i++) {
    items[i].onclick = function() {
      let parent = this.parentElement;
      let label = parent.getElementsByTagName("label");
      
      //zapamietanie poprzednich wartosci i przygotowanie ich do edycji
      let fullText = label[0].innerText;
      let itemName = fullText.replace(/[0-9]/g, "");
      let itemQuantity = fullText.replace(/\D/g, "");

      //stworzenie 2 inputow wraz ze starymi wartosciami
      let editItemQuantity = document.createElement("input");
      let editItemName = document.createElement("input");
      editItemQuantity.value = itemQuantity;
      editItemName.value = itemName;
      parent.replaceChild(editItemQuantity, parent.childNodes[0]);
      parent.insertBefore(editItemName, parent.childNodes[0]);

      //ustawiam widocznosc przyciskow delete i edit na false
      let del = parent.getElementsByClassName("remove");
      let edit = parent.getElementsByClassName("edit");
      edit[0].setAttribute("hidden", true);
      del[0].setAttribute("hidden", true);

      // tworze nowe przyciski
      let saveButton = document.createElement("button");
      saveButton.setAttribute("class", "save");
      saveButton.innerText = "Save";
      parent.insertBefore(saveButton, parent.childNodes[2]);

      let cancelButton = document.createElement("button");
      cancelButton.setAttribute("class", "cancel");
      cancelButton.innerText = "Cancel";
      parent.insertBefore(cancelButton, parent.childNodes[2]);

      cancelButton.onclick = function() {
        cancelEdit(itemName, itemQuantity, parent);
      };

      saveButton.onclick = function() {
        saveEdit(editItemName, editItemQuantity, parent);
      };
    };
  }
}

function cancelEdit(item, quantity, parent) {
  let oldLabel = document.createElement("label");
  oldLabel.textContent = item + " " + quantity;
  //usuwam input
  parent.removeChild(parent.childNodes[0]);
  parent.removeChild(parent.childNodes[0]);
  //wstawiam stary tekst
  parent.insertBefore(oldLabel, parent.childNodes[0]);
  
  //usuwam przyciski cancel i save
  parent.removeChild(parent.childNodes[2]);
  parent.removeChild(parent.childNodes[1]);

  //wlaczam widocznosc del i edit
  let del = parent.getElementsByClassName("remove");
  let edit = parent.getElementsByClassName("edit");
  del[0].removeAttribute("hidden");
  edit[0].removeAttribute("hidden");
}

function saveEdit(item, quantity, parent) {
  let newLabel = document.createElement("label");
  newLabel.textContent = item.value + " " + quantity.value;
  
  parent.removeChild(parent.childNodes[0]);
  parent.removeChild(parent.childNodes[0]);
  
  parent.insertBefore(newLabel, parent.childNodes[0]);
  parent.removeChild(parent.childNodes[2]);
  parent.removeChild(parent.childNodes[1]);

  let del = parent.getElementsByClassName("remove");
  let edit = parent.getElementsByClassName("edit");
  del[0].removeAttribute("hidden");
  edit[0].removeAttribute("hidden");
}

function alreadyBought(item) {
  let boughtItems = document.getElementById("bought-items");
  
  item.removeChild(item.childNodes[1]);
  item.removeChild(item.childNodes[1]);
  let clone = item.cloneNode(true);
  boughtItems.appendChild(clone);
 
  if(localStorage.getItem("boughtCounter") == 0){
    boughtCounter = localStorage.getItem("boughtCounter"); 
  }
  localStorage.setItem("Bought" + boughtCounter, clone.innerText);
  boughtCounter++;
  localStorage.setItem("boughtCounter",boughtCounter);

  const items = { ...localStorage };
  Object.keys(items).forEach(function(item) {
    if (item.startsWith("Item")) {
      if(items[item] == clone.innerText){
        localStorage.removeItem(""+item);
      }
    } 
  });
  
  
}

//ilosc sztuk zamiast ceny
//jesli dodam taki sam produkt zwieksz licznik
//local storage na listy przedmiotow
//console.log(item.childNodes[0]);
  //console.log(item.childNodes[1]);
  //console.log(item.childNodes[2]);
  //czy remove child ma range