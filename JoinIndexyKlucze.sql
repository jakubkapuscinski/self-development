DROP TABLE CHARACTER;
DROP TABLE CARTOON;
/*Klucze*/
CREATE TABLE CARTOON
(
	Id int IDENTITY(1,1) NOT NULL,
	Title varchar(50), 
	Price decimal NOT NULL, 
	CONSTRAINT PK_CARTOON_ID PRIMARY KEY CLUSTERED(Id)
);

CREATE TABLE CHARACTER
(
	    Id int IDENTITY(1,1) NOT NULL,
		Cartoon_Id int,
		Name varchar(25),
		CONSTRAINT PK_CHARACTER_ID PRIMARY KEY  CLUSTERED(Id),
		CONSTRAINT FK_CARTOON FOREIGN KEY (Cartoon_Id) REFERENCES
		CARTOON(Id)
);


INSERT INTO CARTOON VALUES('DragonBall',0);
INSERT INTO CARTOON VALUES('ScoobyDoo',1000);
INSERT INTO CARTOON VALUES('TheSimpsons',12312.2);
INSERT INTO CARTOON VALUES('ThePowerpuffGirls',2132134);
INSERT INTO CARTOON VALUES('ScoobyDoo',312412412);
INSERT INTO CARTOON VALUES('Naruto',24151521);
INSERT INTO CARTOON VALUES('Reksio',421541515);
INSERT INTO CHARACTER VALUES(1,'Vegeta');
INSERT INTO CHARACTER VALUES(1,'Vegeta');
INSERT INTO CHARACTER VALUES(1,'Goku');
INSERT INTO CHARACTER VALUES(2,'Fred');
INSERT INTO CHARACTER VALUES(2,'Shaggy');
INSERT INTO CHARACTER VALUES(null,'Velma');
INSERT INTO CHARACTER VALUES(3,'Bart');
INSERT INTO CHARACTER VALUES(3,'Lisa');
INSERT INTO CHARACTER VALUES(3,'Homer');
INSERT INTO CHARACTER VALUES(null,'Meggy');
INSERT INTO CHARACTER VALUES(4,'Bloosom');
INSERT INTO CHARACTER VALUES(null,'Bubbles');
INSERT INTO CHARACTER VALUES(null,'Buttercup');

SELECT * FROM CARTOON;
SELECT * FROM CHARACTER;

/*TYPY Z��CZE�*/

/*
	INNER JOIN zwraca tylko warto�ci kt�re nie posiadaj� NULL
*/
SELECT *
  FROM CARTOON INNER JOIN CHARACTER
             ON CARTOON.Id = CHARACTER.Cartoon_Id;

SELECT CARTOON.Title, CHARACTER.Name
  FROM CARTOON INNER JOIN CHARACTER
             ON CARTOON.Id = CHARACTER.Cartoon_Id;
 
 /*
	OUTER JOIN:
	(a) Left Join
	(b) Right Join
	(c)	Full Join
*/

/*
	Left Join
wiersze dla kt�rych warunek z��czenia jest spe�niony,
wiersze z �lewej tabeli� dla kt�rych nie ma odpowiednik�w w prawej (CARTOON LEFT JOIN CHARACTER).
*/

SELECT *
  FROM CARTOON LEFT JOIN CHARACTER
             ON CARTOON.Id = CHARACTER.Cartoon_Id;

SELECT CARTOON.Title ,CHARACTER.Name
  FROM CARTOON
       LEFT JOIN CHARACTER
       ON CARTOON.Id = CHARACTER.Cartoon_Id;

 /*
	Right Join
wiersze dla kt�rych warunek z��czenia jest spe�niony,
wiersze z �prawej tabeli� dla kt�rych nie ma odpowiednik�w w lewej (CARTOON RIGHT OUTER JOIN CHARACTER).
*/
SELECT *
  FROM CARTOON RIGHT JOIN CHARACTER
             ON CARTOON.Id = CHARACTER.Cartoon_Id
			 ORDER BY CARTOON.Id ASC;
/*
	Full Join
wiersze dla kt�rych warunek z��czenia jest spe�niony,
wiersze z �lewej tabeli� dla kt�rych nie ma odpowiednik�w w prawej (CARTOON LEFT JOIN CHARACTER),
wiersze z �prawej tabeli� dla kt�rych nie ma odpowiednik�w w lewej (CARTOON RIGHT OUTER JOIN CHARACTER).
*/

SELECT *
  FROM CARTOON FULL OUTER JOIN CHARACTER
             ON CARTOON.Id = CHARACTER.Cartoon_Id
			 ORDER BY CARTOON.Id ASC;


/* Indexy */

CREATE NONCLUSTERED INDEX IX_CHARACTER_NAME ON CHARACTER(NAME);

SELECT * FROM CHARACTER WHERE NAME LIKE 'V%';

SELECT CHARACTER.Name FROM CHARACTER WHERE NAME LIKE 'V%';

/*GROUP BY i HAVING*/

SELECT SUBSTRING(Name, 1,1) Letter, COUNT(Id) as "Amount Of Names" 
FROM CHARACTER
WHERE COUNT(Id) >= 2
GROUP BY SUBSTRING(Name, 1,1);


SELECT SUBSTRING(Name, 1,1) Letter, COUNT(Id) As "Amount Of Names" 
FROM CHARACTER 
GROUP BY SUBSTRING(Name, 1,1)
Having COUNT(Id)  >= 2


/* Transakcje */
BEGIN TRANSACTION BeforeUpdate; /*rozpoczecie*/

BEGIN TRY
	UPDATE CARTOON 
	SET Price = Price / 0   /*wykonanie*/
	WHERE Title = 'DragonBall';
END TRY

BEGIN CATCH

	ROLLBACK TRANSACTION BeforeUpdate; /*zamkniecie*/

END CATCH

COMMIT TRANSACTION BeforeUpdate
/********************************************************************************************************************************/
BEGIN TRANSACTION BeforeUpdate2;

BEGIN TRY
	UPDATE CARTOON 
	SET Price = Price + 1000   /*wykonanie*/	
	WHERE Title = 'ScoobyDoo';
END TRY

BEGIN CATCH

	ROLLBACK TRANSACTION BeforeUpdate2; 

END CATCH

COMMIT TRANSACTION BeforeUpdate2;  /*zamkniecie*/
