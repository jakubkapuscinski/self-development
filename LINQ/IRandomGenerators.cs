﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LINQ
{
    public interface IRandomGenerators
    {
        string RandomString();
        int RandomInt();
    }
}
