﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Authentication.ExtendedProtection;
using System.Text.RegularExpressions;

namespace LINQ
{
    partial class Program
    {
        static void Main(string[] args)
        {
            var serviceCollection = new ServiceCollection();
            serviceCollection.AddTransient<IRandomGenerators, RandomGenerators>();
            var serviceProvider = serviceCollection.BuildServiceProvider();

            List<Student> studendsList = new List<Student>();
            List<string> wordsList = new List<string>() { "H", "Dog", "Cat", "i", "Parrot", "Fog", "Dog", "Parrot" };
            List<string> filterList = new List<string>() { "Parrot", "Dog" };

            for (int i = 0; i < 10; i++)
            {
                var student = ActivatorUtilities.CreateInstance<Student>(serviceProvider);
                studendsList.Add(student);
            }

            var myLinqQuery = from n in studendsList
                              where n.Name.Contains("A")
                              select n.Name; //as query

            var myLinqQuery2 = studendsList
                .Where(n => n.Name.Contains('A')); //as functiodn

            var wordsWithOneLetter = wordsList
                .Where(a => a.Length == 1);

            var wordsContainsSubstring = wordsList
                .Last(w => w.Contains("og"));

            var fifthWord = wordsList
                .ElementAtOrDefault(5);

            var wordsAfterNonCapitalWord = wordsList
                .SkipWhile(w => Regex.IsMatch(w, "[A-Z]"));

            var distinctWords = wordsList
                .Distinct();

            var filteredWords = wordsList
                    .Intersect(filterList);

            Console.WriteLine("myLinqQuery:");
            foreach (var name in myLinqQuery) Console.WriteLine(name + " ");

            Console.WriteLine();
            Console.WriteLine("myLinqQuery2: ");
            Console.WriteLine();
            foreach (var name in myLinqQuery2) Console.WriteLine(name.Name + " ");

            Console.WriteLine();
            Console.WriteLine("wordsWithOneLetter: ");
            Console.WriteLine();
            foreach (var word in wordsWithOneLetter) Console.WriteLine(word);

            Console.WriteLine();
            Console.WriteLine("wordsContainsSubstring: ");
            Console.WriteLine();
            foreach (var word in wordsContainsSubstring) Console.WriteLine(word);

            Console.WriteLine();
            Console.WriteLine("fifthWord: ");
            Console.WriteLine();
            foreach (var word in fifthWord) Console.WriteLine(word);

            Console.WriteLine();
            Console.WriteLine("wordsAfterNonCapitalWord: ");
            Console.WriteLine();
            foreach (var word in wordsAfterNonCapitalWord) Console.WriteLine(word);

            Console.WriteLine();
            Console.WriteLine("distinctWords: ");
            Console.WriteLine();
            foreach (var word in distinctWords) Console.WriteLine(word);

            Console.WriteLine();
            Console.WriteLine("filteredWords: ");
            Console.WriteLine();
            foreach (var word in filteredWords) Console.WriteLine(word);

        }
    }
}
