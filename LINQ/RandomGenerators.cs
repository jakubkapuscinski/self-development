﻿using System;
using System.Linq;

namespace LINQ
{
    public class RandomGenerators : IRandomGenerators
    {
        private const string chars = "ABCDEFGHIJKLMNOPRSTUWXYZ";
        private static Random random = new Random();

        public string RandomString()
        {
            return new string(Enumerable.Repeat(chars, 6)
                .Select(s => s[random.Next(s.Length)])
                .ToArray());
        }

        public int RandomInt()
        {
            return random.Next(1, 21);
        }
    }
}
