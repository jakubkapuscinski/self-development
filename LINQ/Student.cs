﻿namespace LINQ
{
    partial class Program
    {
        public class Student
        {
            private readonly IRandomGenerators _randomGenerators;

            public string Name { get; set; }
            public int Age { get; set; }

            public Student(IRandomGenerators randomGenerators)
            {
                _randomGenerators = randomGenerators;
                Name = _randomGenerators.RandomString();
                Age = _randomGenerators.RandomInt();
            }
        }
    }
}
