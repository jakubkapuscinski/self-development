#include <iostream>
#include <string> 
using namespace std;

template <typename T>
struct List
{
public:
	struct Node
	{
		int value;
		Node* next;
		Node* prev;

		Node(T v, Node* n, Node* p)
		{
			value = stoi(v);
			next = n;
			prev = p;
		}
	};

	Node* head;
	Node* tail;
	int counter;

public:
	List();
	//~List();
	bool isEmpty();
	void Get();
	void Print();
	void PrintBack();
	int GetLength();
	void Sum(List<T> a, List<T> b);
	void Substr(List<T> a, List<T> b);
	void Insert(T v);
	void PrintTail();
};

template <typename T>
List<T>::List()
{
	head = tail = nullptr;
	counter = 0;
}
/*
template <typename T>
List<T>::~List()
{
	while (head != nullptr)
	{
		Node* killer = head;
		head = head->next;
		counter--;

		delete killer;
	}
}
*/
template <typename T>
void List<T>::Print()
{
	if (head == nullptr)
		throw logic_error("Brak argumentow");

	Node* n = head;

	for (int i = 0; i < counter; i++)
	{
		cout << n->value << endl;
		n = n->next;
	}
}

template <typename T>
void List<T>::PrintBack()
{
	Node* n = tail;

	for (int i = counter; i > 0; i--)
	{
		cout << n->value;
		n = n->prev;
	}
}

template <typename T>
void List<T>::Insert(T v)
{
	Node* pred = nullptr;
	Node* succ = head;


	while (succ != nullptr)
	{
		pred = succ;
		succ = succ->next;
	}

	Node* creator = new Node(v, succ, pred);

	if (pred == nullptr)
	{
		head = creator;
		tail = creator;
	}
	else
		pred->next = creator;
	tail = creator;

	counter++;
}

template <typename T>
bool List<T>::isEmpty()
{
	if (head == nullptr)
	{
		cout << 1;
		return true;
	}

	cout << 0;
	return false;
}

template <typename T>
void List<T>::Get()
{
	head = head->next;
	if (head)
	{
		head->prev = nullptr;
	}
	counter--;
}


template <typename T>
void List<T>::PrintTail()
{
	if (tail == NULL)
		throw logic_error("Brak argumentow");

	Node* n = tail;
	cout << n->value << endl;

}

template <typename T>
void List<T>::Sum(List<T> a, List<T> b)
{
	Node* succA = a.tail;
	Node* succB = b.tail;
	int carry = 0, sum;

	while ((succA != NULL) || (succB != NULL))
	{
		sum = carry + (succA ? succA->value : 0) +
			(succB ? succB->value : 0);

		carry = (sum >= 10) ? 1 : 0;

		sum = sum % 10;

		Insert(to_string(sum));

		if (succA) succA = succA->prev;
		if (succB) succB = succB->prev;

	}
	if (carry > 0)
		Insert(to_string(carry));
}

template <typename T>
void List<T>::Substr(List<T> a, List<T> b)
{
	Node* succA;
	Node* succB;
	int carry = 0, diff;
	bool isNegative;

	if (a.counter < b.counter)
	{
		succA = b.tail;
		succB = a.tail;
		isNegative = true;
	}
	else
	{
		succA = a.tail;
		succB = b.tail;
		isNegative = false;
	}

	while ((succA != NULL) && (succB != NULL))
	{
		if (succA->value < succB->value)
		{
			Node* temp = succA->prev;
			while (temp->value == 0)
			{
				temp = temp->prev;
			}
			while (temp != succA)
			{
				temp->value--;
				temp = temp->next;
				temp->value += 10;
			}
		}

		diff = succA->value - succB->value;

		Insert(to_string(diff));

		if (succA) succA = succA->prev;
		if (succB) succB = succB->prev;
	}

	while (succA != NULL)
	{
		Insert(to_string(succA->value));
		succA = succA->prev;
	}

	while (tail->value == 0)
	{
		tail = tail->prev;
		tail->next = nullptr;
		counter--;
	}

	if (isNegative)
	{
		if (counter > 1)
		{
			Node* temp = tail;
			tail = temp->prev;
			tail->next = nullptr;
			counter--;
			Insert("-" + to_string(temp->value));
			temp = nullptr;
		}
		else
		{
			Node* temp = tail;
			Get();
			Insert("-" + to_string(temp->value));
		}
	}
}

template <typename T>
int List<T>::GetLength()
{
	return counter;
}