﻿using AutoMapper;
using NTierArchitecture.BusinessLogic.DTOs;
using NTierArchitecture.BusinessLogic.Interfaces;
using NTierArchitecture.Repository.Interfaces;
using System;

using System.Threading.Tasks;

namespace NTierArchitecture.BusinessLogic.Services
{
    public class Szkalnka
    {

    }

    public class SzklankaFactory
    {
        public Szkalnka CreateSzklanka()
        {
            var szklanka = new Szkalnka();

            return szklanka;
        }
    }



    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserService(IUserRepository userRepository,IMapper mapper)
        {
            _userRepository = userRepository;
            _mapper = mapper;
        }

        public async Task<UserDTO> GetById(int id)
        {
            var user = await _userRepository.GetById(id);

            var userDTO = _mapper.Map<UserDTO>(user);

            return userDTO;
        }
    }
}
