﻿
using NTierArchitecture.ContextData.Entities;
using System.Threading.Tasks;

namespace NTierArchitecture.Repository.Interfaces
{
    public interface IUserRepository
    {
        Task<User> GetById(int id);
    }
}
