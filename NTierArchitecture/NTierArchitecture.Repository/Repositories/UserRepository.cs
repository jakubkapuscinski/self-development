﻿using Microsoft.EntityFrameworkCore;
using NTierArchitecture.ContextData;
using NTierArchitecture.ContextData.Entities;
using NTierArchitecture.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;


namespace NTierArchitecture.Repository.Repositories
{
    public class UserRepository : IUserRepository
    {
        private NTierArchitectureContext _dbContext;
        public UserRepository(NTierArchitectureContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<User> GetById(int id)
        {
            var user = await _dbContext.Users.SingleOrDefaultAsync(x => x.Id == id);
            return user;
        }
    }
}
