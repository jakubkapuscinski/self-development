﻿using System.Collections.Generic;
using System.Linq;

namespace SOLID.DependencyInversion
{
    public class MessageSender
    {
        private IEnumerable<IMessages> _myMessages;

        public MessageSender(IEnumerable<IMessages> myMessages)
        {
            _myMessages = myMessages;
        }

        public void Send()
        {
            _myMessages.AsEnumerable().ToList().ForEach(n => n.SendMessage());
        }
    }
}
