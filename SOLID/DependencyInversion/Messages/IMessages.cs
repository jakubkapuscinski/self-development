﻿namespace SOLID.DependencyInversion
{
    public interface IMessages
    {
        void SendMessage();
    }
}
