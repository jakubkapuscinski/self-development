﻿using System;

namespace SOLID.DependencyInversion.Messages
{
    public class Messages
    {
        public class SMS : IMessages
        {
            public string Number { get; set; }
            public string Content { get; set; }

            public SMS(string number, string content)
            {
                Number = number;
                Content = content;
            }

            public void SendMessage()
            {
                Console.WriteLine("Send to: " + this.Number + "\n Message: " + this.Content + "\n");
            }
        }

        public class MMS : IMessages
        {
            public string Number { get; set; }
            public string Content { get; set; }

            public MMS (string number, string content)
            {
                Number = number;
                Content = content;
            }

            public void SendMessage()
            {
                Console.WriteLine( "Send to: "+ this.Number  + "\n Message: " + this.Content + "\n");
            }
        }

        public class EMAIL : IMessages
        {
            public string EmailAddress { get; set; }
            public string Content { get; set; }

            public EMAIL(string emailAddress, string content)
            {
                EmailAddress = emailAddress;
                Content = content;
            }
            public void SendMessage()
            { 
               Console.WriteLine("Send to: " + this.EmailAddress + "\n Message: " + this.Content + "\n");
            }
        }
    }
}
