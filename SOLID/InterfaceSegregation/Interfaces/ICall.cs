﻿namespace SOLID.InterfaceSegregation
{
    public interface ICall
    {
       void Call(int number);
    }
}
