﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID.InterfaceSegregation.Interfaces
{
    public interface IMakePhoto
    {
        void MakePhoto();
    }
}
