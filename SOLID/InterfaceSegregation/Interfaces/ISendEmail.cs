﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID.InterfaceSegregation.Interfaces
{
    public interface ISendEmail
    {
        void SendEmail(int toWhom, string emailText);
    }
}
