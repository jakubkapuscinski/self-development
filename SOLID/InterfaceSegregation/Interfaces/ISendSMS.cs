﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SOLID.InterfaceSegregation.Interfaces
{
    public interface ISendSMS
    {
        void SendSMS(int phoneNumber, string smsText);
    }
}
