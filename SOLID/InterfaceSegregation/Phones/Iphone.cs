﻿using System;

namespace SOLID.InterfaceSegregation.Interfaces
{

    /// <summary>
    /// nasze klasy będą implementować tylko te interfejsy, których metody będą im potrzebne
    /// </summary>
    public class Iphone : ICall, IMakePhoto, ISendEmail, ISendSMS
    {
        public void Call(int number)
        {
            Console.WriteLine("Calling:" + number);
        }

        public void MakePhoto()
        {
            Console.WriteLine("Photo Saved");
        }

        public void SendEmail(int toWhom, string emailText)
        {
            Console.WriteLine(" Send to: " + toWhom +  "/nMessage:" + emailText);
        } 

        public void SendSMS(int phoneNumber, string smsText)
        {
            Console.WriteLine(" Send to: " + phoneNumber + "/nMessage:" + smsText);
        }
    }
}
