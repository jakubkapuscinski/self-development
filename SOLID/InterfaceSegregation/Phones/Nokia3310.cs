﻿using SOLID.InterfaceSegregation.Interfaces;
using System;

namespace SOLID.InterfaceSegregation
{
    /// <summary>
    /// nasze klasy będą implementować tylko te interfejsy, których metody będą im potrzebne:
    /// </summary>
    public class Nokia3310 : ICall, ISendSMS
    {
        public void Call(int number)
        {
            Console.WriteLine("Calling:" + number);
        }

        public void SendSMS(int phoneNumber, string smsText)
        {
            Console.WriteLine(" Send to: " + phoneNumber + "/nMessage:" + smsText);
        }
    }
}
