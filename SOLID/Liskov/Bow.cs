﻿using System;

namespace SOLID.Liskov
{
    public class Bow: Weapon
    {
        public override void Attack()
        {
            base.Attack();
            Console.WriteLine("I'm shooting with arrow");
        }
    }
}
