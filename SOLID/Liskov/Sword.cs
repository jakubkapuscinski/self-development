﻿using System;

namespace SOLID.Liskov
{
    public class Sword : Weapon
    {
        public override void Attack()
        {
            base.Attack();
            Console.WriteLine("Slicing enemy!");
        }
    }
}
