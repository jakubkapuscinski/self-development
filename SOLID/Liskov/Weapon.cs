﻿using System;

namespace SOLID.Liskov
{
    /// <summary>
    ///  każda funkcja w naszym programie powinna działać w sposób przewidywalny
    ///  staramy sie dziedziczyc po klasie najbardziej ogolnej
    ///   Nasze dziedziczenie musimy zaplanować w taki sposób aby klasa 
    ///   pochodna mogła wykorzystać wszystkie metody klasy bazowej, które implementuje.
    /// </summary>
    public class Weapon
    {
        public string Name { get; set; }
        public virtual void Attack()
        {
            Console.WriteLine("I'm attacking");
        }
    }
}
