﻿using System;

namespace SOLID.OpenClose
{
    public class HouseTypeFactory
    {
        public class SquareHouse : IFacility
        {
            private double Width { get; set; }

            public SquareHouse(double width)
            {
                Width = width;
            }

            public double CalculateSurface()
            {
                return Math.Pow(Width, 2);
            }
        }

        public class TriangleHouse : IFacility
        {
            private double Width { get; set; }
            private double Height { get; set; }

            public TriangleHouse(double height, double width)
            {
                Height = height;
                Width = width;
            }

            public double CalculateSurface()
            {
                return (1 / 2 * Width * Height);
            }
        }

        public class CircleHouse : IFacility
        {
            private double Radius;

            public CircleHouse(double radius)
            {
                Radius = radius;
            }

            public double CalculateSurface()
            {
                return (Math.PI * Math.Pow(Radius, 2));
            }
        }
    }
}
