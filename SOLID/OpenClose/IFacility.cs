﻿namespace SOLID.OpenClose
{
    public interface IFacility
    {
        double CalculateSurface();
    }
}
