﻿using SOLID.DependencyInversion;
using SOLID.DependencyInversion.Messages;
using SOLID.OpenClose;
using SOLID.SingleResponsible;
using System;
using System.Collections.Generic;
using static SOLID.DependencyInversion.Messages.Messages;

namespace SOLID
{
    class Program
    {
        static void Main(string[] args)
        {
           EMAIL[] emailArray = new EMAIL[]
           {
              new EMAIL("jakub@wp.pl", "Hello it's me email1"),
              new EMAIL("kapusta2@wp.pl", "Hello it's me email2")
           };
        
           SMS[] smsArray = new SMS[]
           {
             new SMS("732-234-123","Hello it's me sms1"),
             new SMS("000-000-000", "Hello it's me sms2")
           };

           MessageSender smsSender = new MessageSender(smsArray);
           MessageSender emailSender = new MessageSender(emailArray);

           smsSender.Send();
           emailSender.Send();

           Console.ReadKey();
        }
    }
}
