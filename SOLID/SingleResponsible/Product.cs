﻿using System.Collections.Generic;

namespace SOLID.SingleResponsible
{
    public class Product
    {
        public static int id = 0;
        public int Id { get; set; }
        public string Name{ get; set; }
        public  uint Quantity { get; set; }
        public decimal Price { get; set; }

        public Product(string name, uint quantity, decimal price)
        {
            Id = id;
            Name = name;
            Quantity = quantity;
            Price = price;
            id++;
        }
    }
}
