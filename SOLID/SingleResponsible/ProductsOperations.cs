﻿using System;
using System.Collections.Generic;
using System.Linq;
/// <summary>
///  każda klasa powinna być tworzona w taki sposób aby odpowiadała za jedno zadanie do którego została przeznaczona.
/// </summary>
namespace SOLID.SingleResponsible
{
    public class ProductsOperations
    {
        private List<Product> Products = new List<Product>();

        public void AddProduct(Product product)
        {
            Products.Add(product);
        }

        public void RemoveProduct(Product product)
        {
            Products.Remove(product);
        }

        public void FindProductById(int id)
        {
            var product = Products.Where(n => n.Id == id).Single();
            Console.WriteLine(product.Name);
        }
    }
}
