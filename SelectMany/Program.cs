﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SelectMany
{
    class Program
    {
        static void Main(string[] args)
        {
            List<List<int>> lists = new List<List<int>>
            {
              new List<int> {1, 2, 3},
              new List<int> {12},
              new List<int> {5, 6, 5, 7},
              new List<int> {10, 10, 10, 12}
            };

            IEnumerable<int> result = lists.SelectMany(list => list.Distinct());

            foreach (var x in result)
            {
                Console.WriteLine(x);
            }

        }
    }
}
