﻿using System;
using System.Linq;

namespace Single_VS_SingleOrDefault
{
    class Program
    {
        static void Main(string[] args)
        {

            //tablica typow anonimowych
            var people = new[]
            {
               new  {Name = "Jakub", Age = 22, Email = "jakub@gmail.com" },
               new  {Name = "Kasia", Age = 25, Email = "kasia@gmail.com" },
               new  {Name = "Robert", Age = 31, Email = "robert@wp.pl" },
               new  {Name = "Marysia", Age = 12, Email = "marysia@niepodam.pl" },
               new  {Name = "Jakub", Age = 56, Email = "szymon@wp.pl" }
            };

            //znajdz pojedynczy element Damian 
            try
            {
                var singleOrDefault = people.SingleOrDefault(n => n.Name == "Damian");
                var singleException = people.Single(n => n.Name == "Damian");
            }
            catch(InvalidOperationException)
            {
                Console.WriteLine("Requested object doesn't exist");
            }
            //znajdz pierwszy element Damian 
            try
            {     
                var firstOrDefault = people.FirstOrDefault(n => n.Name == "Damian");
                var first = people.First(n => n.Name == "Damian");
            }
            catch(InvalidOperationException)
            {
                Console.WriteLine("Requested object doesn't exist");
            }
        }
    }
}
