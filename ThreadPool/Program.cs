﻿using System;
using System.Threading;

namespace ThreadPools
{
    class Program
    {

        static void Main(string[] args)
        {
            ThreadPool.QueueUserWorkItem(SomeTask); //dodaj do kolejki SomeTask

            Console.WriteLine("Main thread does some work, then sleeps.");
            Thread.Sleep(500); //kiedy usypiam  glowny watek SomeTask() zaczyna pracę 

            Console.WriteLine("Main thread exits.");
            Console.ReadKey();
        }

        static void SomeTask(Object stateInfo)
        {
            Console.WriteLine("Hello! I'm doing tasks");
            Thread.Sleep(1000);
        }
    }
}
