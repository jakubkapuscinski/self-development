﻿using System;
using System.Threading;

namespace Threads
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Main thread starts here.");
            Thread heavyOperation1 = new Thread(DoSomeHeavyOperation);
            Thread heavyOperation2 = new Thread(DoSomeHeavyOperation2);
            heavyOperation1.Start();
            heavyOperation2.Start();
            DoSomething();
            Console.WriteLine("Main thread ends here.");
            Console.ReadKey();
        }

        public static void DoSomeHeavyOperation()
        {
            Console.WriteLine("I'm doing heavy operation");
            Thread.Sleep(1000);
            Thread.Sleep(1000);
            Console.WriteLine("1!....");
            Thread.Sleep(1000);
            Console.WriteLine("2!....");
            Thread.Sleep(1000);
            Console.WriteLine("3!....");
            Console.WriteLine("I'm HeavyOperation1 and I'm done.");
        }

        public static void DoSomeHeavyOperation2()
        {
            Console.WriteLine("I'm doing heavy operation2");
            Thread.Sleep(1000);
            Thread.Sleep(1000);
            Console.WriteLine("1....");
            Thread.Sleep(1000);
            Console.WriteLine("2....");
            Thread.Sleep(1000);
            Console.WriteLine("3....");
            Console.WriteLine("I'm HeavyOperation2 and I'm done.");
        }
        public static void DoSomething()
        {
            Console.WriteLine("Hey! I'm just counting to 20 !");
            for (int i = 0; i < 20; i++) Console.WriteLine(i);
            Console.WriteLine();
            Console.WriteLine("I'm done.");
        }
    }
}

