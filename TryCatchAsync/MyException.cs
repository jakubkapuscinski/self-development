﻿using System;
namespace TryCatchAsync
{
    public class MyException : Exception
    {
        public MyException(String message) : base(message)
        { }
    }
}
