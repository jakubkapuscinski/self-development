﻿using System;
using System.Threading.Tasks;

namespace TryCatchAsync
{
    class Program
    {
        static void Main(string[] args)
        {
            Task.Run(async () =>
            {
                await TryChatchTasksExamples();
            }).Wait();
        }

        static void WaitAndThrow(int id, int waitInMs)
        {
            Task.Delay(waitInMs);
            Console.WriteLine($"{DateTime.UtcNow}: Task {id} started");
            throw new MyException($"Task {id} throwing at {DateTime.UtcNow}");
        }

        static async Task TryChatchTasksExamples()
        {
            Task[] taskArray = {     Task.Run(() => WaitAndThrow(1, 1000)),
                                     Task.Run(() => WaitAndThrow(2, 5000)),
                                     Task.Run(() => WaitAndThrow(3, 5000))};
            try
            {
                Task.WaitAny(taskArray); //okazuje się że WaitAny nie jest lapane do AggregateException
                //await Task.WhenAny(taskArray);
            }

            catch (AggregateException ex)
            {
                foreach (var exception in ex.InnerExceptions)
                {
                    Console.WriteLine($"Caught AggregateException in Main at {DateTime.UtcNow}: " + exception.Message);
                }
            }

            catch (MyException ex)
            {
                Console.WriteLine($"Caught Exception in Main at {DateTime.UtcNow}: " + ex.Message);
            }

            finally
            {
                foreach (var task in taskArray)
                {
                    if (task.IsFaulted)
                    {
                        foreach (var exception in task.Exception.InnerExceptions)
                        {
                            Console.WriteLine($"Caught AggregateException in Main at {DateTime.UtcNow}: " + exception.Message);
                        }
                    }
                }
            }

            Console.WriteLine("Done.");
            Console.ReadLine();
        }
    }
}

