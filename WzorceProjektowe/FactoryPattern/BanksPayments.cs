﻿using System;

namespace FactoryPattern
{
    public class BanksPayments
    {
        public class BankOne : IPaymentGateway
        {
            public void MakePayment(Product product)
            {
                Console.WriteLine("Pierwszy rodzaj płatności za {0}, kwota {1}", product.Name, product.Price);
            }
        }
        
        public class BankTwo : IPaymentGateway
        {
            public void MakePayment(Product product)
            {
                Console.WriteLine("Drugi rodzaj płatności za {0}, kwota {1}", product.Name, product.Price);
            }
        }
    }
}
