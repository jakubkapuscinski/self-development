﻿namespace FactoryPattern
{
    public enum EPaymentMethod
    {
        BANK_ONE = 0,
        BANK_TWO = 1
    }
}
