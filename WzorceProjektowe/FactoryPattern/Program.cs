﻿using System;

namespace FactoryPattern
{
    class Program
    {
        static void Main(string[] args)
        {
            Product product = new Product("Car","Very Fast",2000);
            
            var gateway = new PaymentGateway();
            
            //w zaleznosci od przekazanego parametru do fabryki tworze nowy obiekt (bramke do platnosci)
            gateway.CreatePaymentGateway(EPaymentMethod.BANK_ONE).MakePayment(product);
        }
    }
}
