﻿using Fasada.Interfaces;
using System;

namespace Fasada.CarParts
{
    public class CarFacade
    {
        private readonly ICarAccessories _accessories;
        private readonly ICarBody _body;
        private readonly ICarEngine _engine;
        private readonly ICarModel _model;

        public CarFacade(ICarAccessories accessories, ICarBody body, ICarEngine engine, ICarModel model)
        {
            _accessories = accessories;
            _body = body;
            _engine = engine;
            _model = model;
        }

        public void CreateCar()
        {
            Console.WriteLine("Creating a Car\n");
            _model.SetModel();
            _engine.SetEngine();
            _body.SetBody();
            _accessories.SetAccessories();

            Console.WriteLine("\nCar created");

        }
    }
}
