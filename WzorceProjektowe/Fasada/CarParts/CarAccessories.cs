﻿using Fasada.Interfaces;
using System;

namespace Fasada.CarParts
{
    public class CarAccessories : ICarAccessories
    {
        public void SetAccessories()
        {
            Console.WriteLine("Setting Accessories");
        }
    }
}
