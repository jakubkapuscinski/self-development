﻿using Fasada.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fasada.CarParts
{
    public class CarEngine : ICarEngine
    {
        public void SetEngine()
        {
            Console.WriteLine("Setting Engine");
        }
    }
}
