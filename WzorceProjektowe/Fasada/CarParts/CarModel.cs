﻿using Fasada.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Fasada.CarParts
{
    public class CarModel : ICarModel
    {
        public void SetModel()
        {
            Console.WriteLine("Setting Model");
        }
    }
}
