﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fasada.Interfaces
{
    public interface ICarAccessories
    {
        void SetAccessories();
    }
}
