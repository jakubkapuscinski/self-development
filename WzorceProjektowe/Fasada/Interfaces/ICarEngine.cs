﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Fasada.Interfaces
{
    public interface ICarEngine
    {
        void SetEngine();
    }
}
