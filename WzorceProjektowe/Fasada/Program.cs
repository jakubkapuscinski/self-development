﻿using Fasada.CarParts;
using System;

namespace Fasada
{
    class Program
    {
        static void Main(string[] args)
        {
            /*
                fasada implementowana jest w postaci jednej klasy ktora jest powiazana z wieloma innymi klasami jakiegos systemu (tutaj tworzenie samochodu)
                otrzymujemy klase ktora ma zdefiniowane funkcjonalnosci CarFacade() i dzieki temu mamy dostep do skladowych jakiegos wiekszego systemu 
            */ 
            var facade = new CarFacade(new CarAccessories(), new CarBody(), new CarEngine(), new CarModel());
            
            facade.CreateCar();

            Console.ReadKey();
        }
    }
}
