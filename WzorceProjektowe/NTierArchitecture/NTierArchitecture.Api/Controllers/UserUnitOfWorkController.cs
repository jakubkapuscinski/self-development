﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using NTierArchitecture.ContextData.Entities;
using NTierArchitecture.Repository.Interfaces;

namespace NTierArchitecture.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UserUnitOfWorkController : ControllerBase
    {
        public IUnitOfWork _unitOfWork { get; private set; }

        public UserUnitOfWorkController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        [HttpGet("{id}")]
        public async Task<User> FlatsAsync(int userId)
        {
            return await _unitOfWork.Users.GetUsersFlatById(userId);
        }
    }
}