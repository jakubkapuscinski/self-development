﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using NTierArchitecture.BusinessLogic.Interfaces;
using NTierArchitecture.BusinessLogic.Services;
using NTierArchitecture.BusinessLogic.Services.Automapper;
using NTierArchitecture.Repository.Interfaces;
using NTierArchitecture.Repository.Repositories;
using NTierArchitecture.Repository.UnitOfWork;

namespace NTierArchitecture.Api.Extensions
{
    public static class ServiceCollectionsExtensions
    {
        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped<IUserRepository, UserRepository>();
        }

       public static void AddServices(this IServiceCollection services)
       {
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
       }

        public static void AddAutoMapper(this IServiceCollection services)
        {
            var configuration = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile<AutomapperProfile>();
            });
            var mapper = configuration.CreateMapper();
            services.AddSingleton(configuration);
            services.AddSingleton(mapper);
        }
    }
}
