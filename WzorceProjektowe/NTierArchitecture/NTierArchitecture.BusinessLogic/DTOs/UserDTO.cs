﻿using NTierArchitecture.ContextData.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace NTierArchitecture.BusinessLogic.DTOs
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public ICollection<Flat> Flats { get; set; }
    }
}
