﻿using NTierArchitecture.BusinessLogic.DTOs;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NTierArchitecture.BusinessLogic.Interfaces
{
    public interface IUserService
    {
        Task<UserDTO> GetById(int id);
    }
}
