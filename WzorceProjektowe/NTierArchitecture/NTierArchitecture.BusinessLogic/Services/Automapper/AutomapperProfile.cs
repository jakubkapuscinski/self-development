﻿using AutoMapper;
using NTierArchitecture.BusinessLogic.DTOs;
using NTierArchitecture.ContextData.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace NTierArchitecture.BusinessLogic.Services.Automapper
{
    public class AutomapperProfile: Profile
    {
        public AutomapperProfile()
        {
            CreateMap<User, UserDTO>().ReverseMap();
        }
    }
}
