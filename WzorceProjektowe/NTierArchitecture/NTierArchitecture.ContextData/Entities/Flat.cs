﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace NTierArchitecture.ContextData.Entities
{
    public class Flat
    {
        [Key]
        public int Id { get; set; }
        public double Height { get; set; }
        public double Length { get; set; }
        public double  Width { get; set; }
        public User User { get; set; }
    }
}
