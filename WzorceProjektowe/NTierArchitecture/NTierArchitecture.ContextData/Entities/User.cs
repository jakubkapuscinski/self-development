﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace NTierArchitecture.ContextData.Entities
{
    public class User
    {
        [Key]
        public int Id { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public ICollection<Flat> Flats{ get; set; }
    }
}
