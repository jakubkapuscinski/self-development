﻿using Microsoft.EntityFrameworkCore;
using NTierArchitecture.ContextData.Entities;

namespace NTierArchitecture.ContextData
{
    public class NTierArchitectureContext: DbContext
    {

        public NTierArchitectureContext(DbContextOptions<NTierArchitectureContext> options) : base(options)
        {

        }
        public DbSet<User> Users { get; set; }
        public DbSet<Flat> Flats { get; set; }
    }
}
