﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NTierArchitecture.Repository.Interfaces
{
    public interface IUnitOfWork: IDisposable
    {
        IUserRepository Users { get;}
        IFlatRepository Flats { get;}
        int Save();
    }
}
