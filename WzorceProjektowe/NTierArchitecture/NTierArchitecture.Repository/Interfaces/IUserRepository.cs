﻿
using NTierArchitecture.ContextData.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace NTierArchitecture.Repository.Interfaces
{
    public interface IUserRepository : IGenericRepository<User>
    {
        Task<IEnumerable<User>> GetAllUsersAsync();
        Task<User> GetUsersFlatById(int userId);
        Task<User> GetUserByIdAsync(int userId);
        Task<User> GetUserWithDetailsAsync(int userId);
        void CreateUser(User user);
        void UpdateUser(User user);
        void DeleteUser(User user);
    }
}
