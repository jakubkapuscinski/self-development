﻿using NTierArchitecture.ContextData;
using NTierArchitecture.ContextData.Entities;
using NTierArchitecture.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace NTierArchitecture.Repository.Repositories
{
    public class FlatRepository: GenericRepository<FlatRepository>, IFlatRepository
    {
        private NTierArchitectureContext _dbContext;
        public FlatRepository(NTierArchitectureContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }
    }
}
