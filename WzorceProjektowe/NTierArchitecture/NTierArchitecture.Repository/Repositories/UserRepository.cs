﻿using Microsoft.EntityFrameworkCore;
using NTierArchitecture.ContextData;
using NTierArchitecture.ContextData.Entities;
using NTierArchitecture.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace NTierArchitecture.Repository.Repositories
{
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        private NTierArchitectureContext _dbContext;
        public UserRepository(NTierArchitectureContext dbContext) : base(dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IEnumerable<User>> GetAllUsersAsync()
        {
            return await FindAllAsync();
        }

        public async Task<User> GetById(int id)
        {
            var user = await _dbContext.Users.SingleOrDefaultAsync(x => x.Id == id);
            return user;
        }

        public async Task<User> GetUserByIdAsync(int userId)
        {
            return await FindByCondition(user => user.Id.Equals(userId))
               .FirstOrDefaultAsync();
        }

        public async Task<User> GetUserWithDetailsAsync(int userId)
        {
            return await FindByCondition(user => user.Id.Equals(userId))
                .Include(user => user.Name)
                .FirstOrDefaultAsync();
        }

        public void UpdateUser(User user)
        {
            Update(user);
        }
       
        public void CreateUser(User user)
        {
            Create(user);
        }
        
        public void DeleteUser(User user)
        {
            Delete(user);
        }

        public async Task<User> GetUsersFlatById(int userId)
        {
            var userWithFlats = await FindByCondition(user => user.Id.Equals(userId))
               .Include(user => user.Flats)
               .FirstOrDefaultAsync();
            
            return userWithFlats;
        }
    }
}
