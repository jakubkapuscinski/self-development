﻿using NTierArchitecture.ContextData;
using NTierArchitecture.ContextData.Entities;
using NTierArchitecture.Repository.Interfaces;
using NTierArchitecture.Repository.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace NTierArchitecture.Repository.UnitOfWork
{
    public class UnitOfWork : IUnitOfWork
    {
        private readonly NTierArchitectureContext _dbContext;
        public IUserRepository Users { get; private set; }
        public IFlatRepository Flats { get; private set; }
        public UnitOfWork(NTierArchitectureContext dbContext)
        {
            _dbContext = dbContext;
            Users = new UserRepository(_dbContext);
            Flats = new FlatRepository(_dbContext);
        }

        public int Save()
        {
            return _dbContext.SaveChanges();
        }

        public void Dispose()
        {
            _dbContext.Dispose();
        }
    }
}
