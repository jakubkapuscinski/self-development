﻿using System;

namespace Singleton
{
    /// <summary>
    ///  czasami nie ma sensu tworzyc instancji komponentu wiecej niz jeden raz na zycie programu 
    ///  dlatego jesli wiemy ze cos raczej sie nie zmieni dobrze jest stworzyc ten obiekt tylko raz
    ///  i korzystac z jego danych przez caly ten okres
    /// </summary>
    class Program
    {
        static void Main(string[] args)
        {
            var db = SingletonDataContainer.Instance;
            Console.WriteLine(db.GetPopulation("Warsaw"));
            
            var db2 = SingletonDataContainer.Instance;
            Console.WriteLine(db2.GetPopulation("London"));

            var db3 = SingletonDataContainer.Instance;
            Console.WriteLine(db2.GetPopulation("New Delhi"));

        }
    }
}
