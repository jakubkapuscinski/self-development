﻿using System;
using System.Threading.Tasks;

namespace asyncTask__VS_asyncVoid
{
   class Program
    {
        static void Main(string[] args)
        {
            CatchAsyncException().ConfigureAwait(false);
            ///CatchAsyncExceptionVoid();
            Console.WriteLine("Ended...");
            Console.ReadKey();
        }

        /// <summary>
        /// async void jest bardzo niebezpieczny ponieważ
        /// nie mamy pojęcia kiedy nasz kod sie zakonczy
        /// zwracanie Task umozliwia nam sledzenie tego watku
        /// </summary>
        public static async void DoAsyncWrong()
        {
            await Task.Run(() =>
            {
                Task.Delay(2000);
                throw new Exception();
            });
        }

        /// <summary>
        /// DoAsync utworzy sobie nowy watek i zacznie wykonywac na nim kod
        /// ale jako ze nie zwraca watku (co robi Task) nie mamy mozliwosci 
        /// uzycia await albo Wait()
        /// </summary>
        private static async void CatchAsyncExceptionVoid()
        {
            try
            {
                DoAsyncWrong();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
        
        public static async Task DoAsyncRight()
        {
            await Task.Run(() =>
            {
                Task.Delay(2000);
                throw new Exception();
            });
        }
       
        private static async Task CatchAsyncException()
        {
            try
            {
                await DoAsyncRight();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
        }
    }
}
